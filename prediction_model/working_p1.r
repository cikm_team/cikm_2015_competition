
#need to improve logloss function
#we can change the limits in logloss functio to increase the accuracy results 
#I calculated the accuracy for t1 in each match and printed them (No value & Yes value)
#from these prediction i know which team will win based on my prediction
#I copied these results into calculating_accuracy.csv file
#I compared how many Predictions of mine are correctly matched with the actual data 
#I also written  some macros function in excel u can see by clicking on the cell
#accuracy results will be seen in sheet2 of csv file


rm(list=ls())
setwd('C:/Users/Sravan Reddy/Desktop/class_project/prediction/Sportsbet_AFL_Competition-master/Sportsbet_AFL_Competition-master')
# 0. Read the Data
cleteams <- read.csv("data/teams.csv", stringsAsFactors = F)
seasons <- read.csv("data/seasons.csv", stringsAsFactors = F)
unplayed <- read.csv("data/unplayed.csv", stringsAsFactors = F)

# getting the  Training Data
# training data only from the 2011 to 2014
train_df <- seasons[which(seasons$season %in% c(2011:2014)),]
#ignore drawn matches
train_df[which(train_df$margin == 0),]

train_df <- train_df[which(train_df$margin > 0),]

train_df$prob <- ifelse(train_df$tid1 == train_df$win_tid, 1,0)
# Extracting features newly 
feat_cols <- c("mid", "prob", "round", "tid1", "tid2", "tid1_loc")
train_df <- train_df[,feat_cols]
# Co-erce the round values from strings to integers (for later use as an explicit ordinal feature):
train_df$round <- as.integer(substr(train_df$round,2,length(train_df$round)))

train_df$tid1 <- as.factor(train_df$tid1)
train_df$tid2 <- as.factor(train_df$tid2)
train_df$tid1_loc <- as.factor(train_df$tid1_loc)

train_df$prob <- ifelse(train_df$prob == 1, 'Yes', 'No')


library(caret)
# y: prob | x: -prob, -mid
trainIdx <- createDataPartition(train_df$prob, p = .7,list = FALSE)
train <- train_df[trainIdx,-1]
test <- train_df[-trainIdx,]

fitControl <- trainControl(method = "adaptive_cv",
                           number = 10,
                           repeats = 5,
                           classProbs = TRUE,
                           summaryFunction = twoClassSummary,

                           adaptive = list(min = 10,
                                           alpha = 0.05,
                                           method = "gls",
                                           complete = TRUE))

set.seed(825)
fit <- train(y=train$prob, x=train[,-1], data=train,
             method = "rf",
             trControl = fitControl,
             preProc = c("center", "scale"),
             tuneLength = 8,
             metric = "ROC")

# Testing 
p <- predict(fit, test, type='prob')

LogLoss<-function(actual, predicted)
{
  predicted<-(pmax(predicted, 0.00001))
  predicted<-(pmin(predicted, 0.99999))
  result<- -1/length(actual)*(sum((actual*log(predicted)+(1-actual)*log(1-predicted))))
  return(result)
}

LogLoss(as.numeric(test$prob)-1,as.numeric(p)-1)




